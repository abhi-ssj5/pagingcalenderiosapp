//
//  ViewController.swift
//  Task7 paging calaender
//
//  Created by Sierra 4 on 22/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SVProgressHUD

class ViewController: UIViewController {
    
    @IBOutlet weak var lblToday: UILabel!

    @IBOutlet var lblDay: [UILabel]!
    @IBOutlet var imgDay: [UIImageView]!
    
    @IBOutlet weak var stackDays: UIStackView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var btnAddSchedule: UIButton!
    
    var days:[String] = ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"]
    
    var param:[String: String] = ["access_token": "ymaANbhfJT4ARby5IbK2u0hUJQ9T7dk8",
                                  "page_no": "1",
                                  "page_size": "7",
                                  "last_date": "",
                                  "date_selected": ""]
    
    var schedules:[Data] = []
    var pageNum:Int = 0
    var temp:Int = 0
    var lastDate:String = ""
    var current_date:String = ""
    var current_cell_offset: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //fetch day, date, month
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let today = calendar.component(.day, from: date)
        let day = calendar.component(.weekday, from: date)
        rearrangeDays(&days, firstIndex: (day - 1))
        assignDays()
        current_date = "\(year)-\(month)-\(today)"
        
        //current date for api
        fetchDataForAWeek(page_no: "1", date_selected: current_date, last_date: "" )
    }
    
    func rearrangeDays(_ array: inout [String], firstIndex: Int) {
        var temp:String = ""
        for _ in 0..<firstIndex {
            temp = array.remove(at: 0)
            array.append(temp)
        }
    }
    
    func assignDays() {
        for index in 0..<7 {
            lblDay[index].text = days[index]
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnCalenderAction(_ sender: Any) {
        collectionView.contentOffset.x = 0
    }
    
    func fetchDataForAWeek(page_no: String, date_selected: String, last_date: String) {
        param["date_selected"] = date_selected
        param["last_date"] = last_date
        param["page_no"] = page_no
        
        SVProgressHUD.show()
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.flat)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        
        ApiHandler.fetchData(parameters: param) { (jsonData) in
            let scheduleModel = Mapper<Instructor>().map(JSONObject: jsonData)
            print(scheduleModel?.msg ?? "")
            
            self.schedules = self.schedules + (scheduleModel?.data)!  //data contains "data"
            self.collectionView.reloadData()
            self.lastDate = self.schedules[self.schedules.count - 1].date1!
        }
        
        SVProgressHUD.dismiss()
    }
    
    func resetAllDayFields() {
        for index in 0..<7 {
            lblDay[index].font = UIFont.systemFont(ofSize: 12, weight: 0)
            imgDay[index].image = UIImage(named: "")
        }
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return schedules.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath as IndexPath) as! CollectionViewCell
        cell.cellData = schedules[indexPath.row].details
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       let heightValue = self.view.frame.height - self.lblToday.frame.height - self.stackDays.frame.height - self.btnAddSchedule.frame.height
        return CGSize(width: self.collectionView.frame.width , height: heightValue)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }

    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView.contentOffset.x == 0 {
            self.resetAllDayFields()
            self.lblToday.text = "Today , " + DateFormat.dateFormat(schedules[0].date!)
            self.lblDay[ 0 ].font = UIFont.systemFont(ofSize: 16, weight: 2)
            self.imgDay[ 0 ].image = UIImage(named: "ic_eject_white")
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        var visible = CGRect()
        visible.origin = collectionView.contentOffset
        visible.size = collectionView.bounds.size
        let visiblePoint = CGPoint(x: visible.midX, y: visible.midY)
        let visibleIndexPath  = collectionView.indexPathForItem(at: visiblePoint)
        current_cell_offset = visibleIndexPath![1]
        if visibleIndexPath == [0,schedules.count - 2] {
            pageNum = pageNum + 1
        }
        if pageNum > temp {
            temp = pageNum
            print(temp)
            fetchDataForAWeek(page_no: "\(temp + 1)", date_selected: self.current_date, last_date: lastDate)
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        var visible = CGRect()
        visible.origin = targetContentOffset.pointee
        visible.size = collectionView.bounds.size
        let visiblePoint = CGPoint(x: visible.midX, y: visible.midY)
        let visibleIndexPath  = collectionView.indexPathForItem(at: visiblePoint)
        
        self.resetAllDayFields()
        if visibleIndexPath![1] == 0 {
            self.lblToday.text = "Today , " + DateFormat.dateFormat(schedules[0].date!)
        } else {
            self.lblToday.text = DateFormat.dateFormat(schedules[visibleIndexPath![1]].date!)
        }
        self.lblDay[ visibleIndexPath![1] % 7 ].font = UIFont.systemFont(ofSize: 16, weight: 2)
        self.imgDay[ visibleIndexPath![1] % 7 ].image = UIImage(named: "ic_eject_white")
    }
  
}

