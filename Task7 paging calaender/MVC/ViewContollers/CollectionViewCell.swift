//
//  CollectionViewCell.swift
//  Task7 paging calaender
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    var cellData:[Details]?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
}

extension CollectionViewCell: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (cellData?.count ?? 0)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath) as! TableViewCell
        
        if indexPath.row == 0 {
            cell.lineAboveGreenDot.backgroundColor = UIColor(red: 1,green: 1,blue: 1, alpha: 1)
        } else {
            cell.lineAboveGreenDot.backgroundColor = UIColor(red: 0.83,green: 0.83,blue: 0.83, alpha: 1)
        }
        
        if indexPath.row < (cellData?.count)! {
            cell.tableCell = (cellData?[indexPath.row])!
            if indexPath.row == (cellData?.count)! - 1 {
                cell.lineBelowFreenDot.backgroundColor = UIColor(red: 1,green: 1,blue: 1, alpha: 1)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
}










