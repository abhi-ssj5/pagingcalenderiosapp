//
//  commonFunction.swift
//  Task7 paging calaender
//
//  Created by Sierra 4 on 24/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation

class DateFormat {
    
    class func dateFormat(_ value: String) -> String{
        
        let months:[String] = ["January",
                               "February",
                               "March",
                               "April",
                               "May",
                               "June",
                               "July",
                               "August",
                               "September",
                               "October",
                               "November",
                               "December"]
        
        let index = value.index(value.startIndex, offsetBy: 3)
        let mm = value.substring(to: index)
        let dd = value.substring(from: index)
        switch mm {
        case "Jan":
            return months[0] + dd
        case "Feb":
            return months[1] + dd
        case "Mar":
            return months[2] + dd
        case "Apr":
            return months[3] + dd
        case "May":
            return months[4] + dd
        case "Jun":
            return months[5] + dd
        case "Jul":
            return months[6] + dd
        case "Aug":
            return months[7] + dd
        case "Sep":
            return months[8] + dd
        case "Oct":
            return months[9] + dd
        case "Nov":
            return months[10] + dd
        case "Dec":
            return months[11] + dd
        default:
            return "MM-DD"
        }
    }

}
