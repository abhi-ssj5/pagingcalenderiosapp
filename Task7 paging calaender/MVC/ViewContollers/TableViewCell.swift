//
//  TableViewCell.swift
//  Task7 paging calaender
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var lineAboveGreenDot: UIView!
    
    @IBOutlet weak var lineBelowFreenDot: UIView!
    
    @IBOutlet weak var greenDot: UIView!
    @IBOutlet weak var scheduleDisplay: UIView!
    
    @IBOutlet weak var lblSubjectName: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblEnrolled: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblFeePerStudent: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblHrs: UILabel!
    @IBOutlet weak var lblMins: UILabel!
    
    
    var tableCell:Details? {
        didSet{
            updateCell()
        }
    }
    
    func updateCell() {
        lblSubjectName.text = (tableCell?.subjects?[0].subject_name)! + " +\((tableCell?.subjects?.count)! - 1)"
        lblDuration.text = tableCell?.time_duration
        lblAddress.text = ": " + (tableCell?.day_location)!
        lblEnrolled.text = "\((tableCell?.enrolled)!)" + "/" + "\((tableCell?.max_student)!)"
        lblAge.text = tableCell?.age_group
        lblFeePerStudent.text = tableCell?.charge_student
        let index = tableCell?.start_time1?.index((tableCell?.start_time1?.startIndex)!, offsetBy: 2)
        lblHrs.text = tableCell?.start_time1?.substring(to: index!)
        lblMins.text = tableCell?.start_time1?.substring(from: index!)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        greenDot.layer.cornerRadius = greenDot.frame.height / 2
        scheduleDisplay.layer.shadowColor = UIColor.black.cgColor
        scheduleDisplay.layer.shadowOffset = CGSize(width: 0.0, height: 0.3)
        scheduleDisplay.layer.shadowOpacity = 0.2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
