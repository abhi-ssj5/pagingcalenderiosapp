//
//  locations.swift
//  Task7 paging calaender
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import ObjectMapper

class Locations: Mappable{
    var slot_day: String?
    var slot_location: Int?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        slot_day <- map["slot_day"]
        slot_location <- map["slot_location"]
    }
}
