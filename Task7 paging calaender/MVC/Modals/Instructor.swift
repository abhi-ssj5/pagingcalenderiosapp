//
//  Instructor.swift
//  Task7 paging calaender
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import ObjectMapper

class Instructor: Mappable{
    var update_popup: Int?
    var status_code: Int?
    var is_package: Int?
    var data: [Data]?
    var msg: String?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        update_popup <- map["update_popup"]
        status_code <- map["status_code"]
        is_package <- map["is_package"]
        data <- map["data"]
        msg <- map["msg"]
    }
}
