//
//  data.swift
//  Task7 paging calaender
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import ObjectMapper

class Data: Mappable{
    var date: String?
    var date1: String?
    var day: String?
    var details: [Details]?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        date <- map["date"]
        date1 <- map["date1"]
        day <- map["day"]
        details <- map["details"]
    }
}
